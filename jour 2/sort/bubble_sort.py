import sort

f = open("text.txt", "r")

list = f.read()
listIndex = 0
arrayList = []

while listIndex < len(list):
    if list[listIndex] == "-":
        nbrLen = 1
        nbr = ""
        while list[listIndex + nbrLen] != " " and list[listIndex + nbrLen] != "\n":
            nbr = nbr + list[listIndex + nbrLen]
            nbrLen += 1

        if nbr != "":
            arrayList.append(int("-" + nbr))
        listIndex += nbrLen
    elif list[listIndex] != " " and list[listIndex] != "\n":
        nbrLen = 0
        nbr = ""
        while list[listIndex + nbrLen] != " " and list[listIndex + nbrLen] != "\n":
            nbr = nbr + list[listIndex + nbrLen]
            nbrLen += 1

        if nbr != "":
            arrayList.append(int(nbr))
        listIndex += nbrLen

    listIndex += 1

list = sort.mySort(arrayList)

print(list)


