def mySort(arrayList):
    arrayLen = len(arrayList)

    for i in range(arrayLen - 1, 0, -1):
        for item in range(i):
            if arrayList[item] > arrayList[item + 1]:
                current = arrayList[item]
                arrayList[item] = arrayList[item + 1]
                arrayList[item + 1] = current

    return arrayList
