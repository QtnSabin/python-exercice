import unittest
import sys
sys.path.append('/Users/quentinsabin/Documents/ecole/IMIE/Cours/Deuxieme_Annee/Python/Exercice/sort/')
import mysplit
import randomString

class TestSplitMethods(unittest.TestCase):
    def setUp(self):
        self.firstParam = randomString.random_string(1000)
        self.secondParam = ' '

    def test_return_array(self):
        self.assertEqual(type(mysplit.mySplit(self.firstParam, self.secondParam)), type([]))

    def test_first_param_isstring(self):
        self.assertEqual(type(self.firstParam), type(''))

    def test_first_param_contain_second_param(self):
        self.assertIn(self.secondParam, self.firstParam)

    def test_return_if_first_param_not_contain_second_param(self):
        secondParam = randomString.random_string(1001)
        self.assertNotIn(secondParam, self.firstParam)
        self.assertEqual(mysplit.mySplit(self.firstParam, secondParam), [self.firstParam])

    def test_return_array_not_contain_second_param(self):
        self.assertNotIn(self.secondParam, mysplit.mySplit(self.firstParam, self.secondParam))

if __name__ == '__main__':
    unittest.main()