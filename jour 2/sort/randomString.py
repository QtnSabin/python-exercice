import string, random

def random_string(N):
    return ''.join(random.choice(string.ascii_lowercase + ' ') for i in range(N))