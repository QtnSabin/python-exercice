import unittest
import tdd2 as tdd


class TestTDD2Methods(unittest.TestCase):
    def test_return_two_param(self):
        self.assertEqual(tdd.tdd2([1,2], 2), 1)

    def test_return_three_param(self):
        self.assertEqual(tdd.tdd2(['hihi', 2,3,4,'hihi'], 'hihi', 2), 4)


if __name__ == '__main__':
    unittest.main()